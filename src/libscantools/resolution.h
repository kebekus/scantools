/*
 * Copyright © 2017 - 2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef RESOLUTION
#define RESOLUTION

#include <QMetaType>
#include <QString>
#include <QtGlobal>

#include "length.h"


/** The resolution class stores a resolution and converts between units
    
    This is a trivial class that helps with resolution storage and
    conversions. Resolutiones are stored intenally in 1/100 dot/cm, which allows
    conversions between units without rounding error.

    The class "resolution" is known to the QMetaType systen and can be used as a
    custom type in QVariant.
*/

class resolution
{
  /** Find the maximum of two resolutions

      @param res1 First resolution

      @param res2 Second resolution

      @return Largest resolution
   */
  friend resolution qMax(const resolution res1, const resolution res2);

 public:
  /** List of supported units */
  enum unit {
    dpcm,   /**< Pixel per Centimeter */
    dpi,    /**< Pixel per Inch */
    dpm,    /**< Pixel per Millimeter */
  };
  
  /** Constructs a zero resolution */
  resolution() : _resolution(0) {
  }

  /** Constructs resolution of given value and unit
      
      @param l Length
      
      @param u Unit
  */
  resolution(qreal l, unit u) {
    set(l, u);
  }
  
  /** Get numerical value for resolution

      @param u Unit
      
      @returns resolution in given unit */
  qreal get(unit u) const;

  /** Sets resolution in given unit

      @param l Length
      
      @param u Unit
   */
  void set(qreal l, unit u);

  /** Check if resolution is zero or less

      @returns true if resolution is zero or less
  */
  bool isNonPositive() const {
    return (_resolution <= 0);
  }

  /** Fuzzy check if resolution is zero
      
      @returns true if resolution is zero
  */
  bool isZero() const {
    return  qFuzzyIsNull(_resolution);
  }

  /** Check if resolution is valid

      @returns true if the resolution is bounded by minResDPI and maxResDPI
  */
  bool isValid() const {
    qreal ridpi = get(dpi);
    return (ridpi >= minResDPI) && (ridpi <= maxResDPI);
  }
  
  /** Convert to string

      @returns resolution in dpi as a human-readable string */
  operator QString() const {
    return QString("%1dpi").arg(get(dpi));
  }

  /** Check for equality 

      @param other Other resolution

      @returns True iff resolutions are equal.
  */
  bool operator==(const resolution other) const
  {
    return _resolution == other._resolution;
  }

  /** Minimal resolution handled by scantools
      
      All classes, functions and methods of this library require that all
      numbers describing graphical resolution are larger than or equal to
      minRes, which is specified in dots per inch.
  */ 
  static const int minResDPI = 10;
  
  /** Maximal resolution handled by scantools
      
      All classes, functions and methods of this library require that all
      numbers describing graphical resolution are smaller than or equal to
      maxRes, which is specified in dots per inch.
  */ 
  static const int maxResDPI = 10000;

  
 private:
  // Resolution in 1/100 mm
  qreal _resolution;
};


/**  Divide a scalar ("number of dots") by a resolution to obtain a length

     @param numerator Scalar ("Number of dots")

     @param denominator Resolution

     @returns length

     \relates resolution */

inline length operator/(qreal numerator, const resolution denominator) {
  return length(numerator/denominator.get(resolution::dpi), length::in);
}

/** Divide a scalar by a length to obtain a resolution

    @param numerator Scalar ("Number of dots")

    @param denominator length
    
    @returns Resolution

    \relates resolution
*/

inline resolution operator/(qreal numerator, const length denominator) {
  return resolution(numerator/denominator.get(length::in), resolution::dpi);
}

/** \relates resolution Maximum of two resolutions */

inline resolution qMax(const resolution res1, const resolution res2)
{
  return res1._resolution > res2._resolution ? res1 : res2;
}


// Makes "resolution" known to QMetaType
Q_DECLARE_METATYPE(resolution)

#endif
