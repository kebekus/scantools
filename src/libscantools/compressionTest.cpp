/*
 * Copyright © 2017 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QProcess>

#include "bitVector.h"
#include "compression.h"
#include "compressionTest.h"
#include "TIFFReader.h"

QTEST_MAIN(compressionTest)

QByteArray compressionTest::referenceFAXEncoder(const QImage &image)
{
  // Save image
  QTemporaryFile imageFile;
  imageFile.open();
  imageFile.close();
  
  image.save(imageFile.fileName(), "tif");

  
  // Convert image to FAX G4 encoded TIFF
  QTemporaryFile tmpFAXTIFFFile;
  tmpFAXTIFFFile.open();
  tmpFAXTIFFFile.close();
  
  QProcess tiffcp;
  tiffcp.start("tiffcp", QStringList() << "-c" << "g4" << imageFile.fileName() << tmpFAXTIFFFile.fileName());
  tiffcp.waitForStarted();
  tiffcp.waitForFinished();
  Q_ASSERT(tiffcp.exitStatus() == QProcess::NormalExit);
  Q_ASSERT(tiffcp.exitCode() == 0);

  // Convert FAX G4 encoded TIFF to PDF
  QTemporaryFile PDFFile;
  PDFFile.open();

  QProcess tiff2pdf;
  tiff2pdf.start("tiff2pdf", QStringList() << tmpFAXTIFFFile.fileName() << "-o" << PDFFile.fileName());
  tiff2pdf.waitForStarted();
  tiff2pdf.waitForFinished();
  Q_ASSERT(tiff2pdf.exitStatus() == QProcess::NormalExit);
  Q_ASSERT(tiff2pdf.exitCode() == 0);

  QByteArray PDF = PDFFile.readAll();
  PDFFile.close();

  
  // Extract FAX code from PDF
  int startIndex = PDF.lastIndexOf("\nstream\n");
  int endIndex = PDF.indexOf("endstream", startIndex);
  Q_ASSERT(startIndex != -1);
  Q_ASSERT(endIndex != -1);
  
  return PDF.mid(startIndex+8, endIndex-startIndex-9);
}


bool compressionTest::bisectAndAnalyseFAXCompressionError(QImage &image)
{

  QByteArray encodedData = compression::faxG4Encode(image);
  QByteArray referenceEncodedData = referenceFAXEncoder(image);

  if (encodedData == referenceEncodedData)
    return false;

  if (image.height() > 2) {
    int verticalCut = image.height()/2;
    QImage i1 = image.copy(0, 0, image.width(), qMax(verticalCut,2));
    if (image.height()-verticalCut < 2)
      verticalCut = qMin(verticalCut,1);
    QImage i2 = image.copy(0, verticalCut, image.width(), image.height());
    
    if (bisectAndAnalyseFAXCompressionError(i1))
      return true;
    if (bisectAndAnalyseFAXCompressionError(i2))
      return true;
  }

  if (image.width() > 2) {
    int verticalCut = image.height()/2;
    QImage i1 = image.copy(0, 0, qMax(verticalCut,2), image.height());
    if (image.height()-verticalCut < 2)
      verticalCut = qMin(verticalCut,1);
    QImage i2 = image.copy(verticalCut, 0, image.width(), image.height());
    
    if (bisectAndAnalyseFAXCompressionError(i1))
      return true;
    if (bisectAndAnalyseFAXCompressionError(i2))
      return true;
  }

  qWarning() << "Minimal example has width" << image.width() << "and height" << image.height();
  for(int i=0; i<image.height(); i++) {
    QByteArray ba((const char *)image.scanLine(i), image.bytesPerLine());
    bitVector bv(ba);
    bv.resize(image.width());
    qWarning() << "Scanline              :" <<  bv.operator QString();
  }
  qWarning() << "FAX encoded data      :" <<  bitVector(encodedData).operator QString();
  qWarning() << "Reference encoded data:" <<  bitVector(referenceEncodedData).operator QString();

  return true;
}


void compressionTest::faxG4Compression_data()
{
  QTest::addColumn<QImage>("image");
  QTest::addColumn<QByteArray>("reference");


  QTest::addColumn<QImage>("image");
  
  QDir dir(QFINDTESTDATA("testData/images/TIFF"));
  QFileInfoList files = dir.entryInfoList( QStringList() << "*.tiff" << "*.tif");
  foreach(const QFileInfo &file, files) {
    TIFFReader doc(file.absoluteFilePath());
    QVERIFY(!doc.hasError());
    for(quint32 i=0; i<doc.size(); i++)
      QTest::newRow(QString("%1 page %2").arg(file.fileName()).arg(i+1).toLocal8Bit()) << doc[i].convertToFormat(QImage::Format_Mono);
  }
}


void compressionTest::faxG4Compression()
{
  QFETCH(QImage, image);

  // Compress image and compare to reference
  QByteArray encodedData = compression::faxG4Encode(image);
  QByteArray reference   = referenceFAXEncoder(image);
  
  if (encodedData != reference) {
    // Error encoding FAX data. Call the method
    // bisectAndAnalyseFAXCompressionError() which produces an image of minimal
    // size that reproduces the error, and prints useful debugging output
    qWarning() << "FAX encoder failed";
    bisectAndAnalyseFAXCompressionError(image);
  }
  QCOMPARE(encodedData, reference);

  // Decompress data, and check with the original image
  QCOMPARE(image, compression::faxG4Decode(reference, image.width(), image.height()));
}

void compressionTest::zlibCompression_data()
{
 QTest::addColumn<QString>("path");

 QDir dir(QFINDTESTDATA("testData/images/TIFF"));
 foreach(const QFileInfo &file, dir.entryInfoList(QDir::Files))
   QTest::newRow(file.absoluteFilePath().toLocal8Bit().constData()) << file.absoluteFilePath();
}

void compressionTest::zlibCompression()
{
  QFETCH(QString, path);
  
  QFile file(path);
  file.open(QIODevice::ReadOnly);
  QByteArray original = file.readAll();
  QByteArray compressed = compression::zlibCompress(original);
  QByteArray decompressed = compression::zlibUncompress(compressed, original.size());

  QCOMPARE(original, decompressed);
}

void compressionTest::zopfliCompression_data()
{
 QTest::addColumn<QString>("path");

 QDir dir(QFINDTESTDATA("testData/images/TIFF"));
 foreach(const QFileInfo &file, dir.entryInfoList(QDir::Files))
   QTest::newRow(file.absoluteFilePath().toLocal8Bit().constData()) << file.absoluteFilePath();
}

void compressionTest::zopfliCompression()
{
  QFETCH(QString, path);
  
  QFile file(path);
  file.open(QIODevice::ReadOnly);
  QByteArray original = file.readAll();
  QByteArray compressed = compression::zopfliCompress(original);
  QByteArray decompressed = compression::zlibUncompress(compressed, original.size());

  QCOMPARE(original, decompressed);
}
