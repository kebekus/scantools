# The library requires a few other packages

FIND_PACKAGE(Jbig2dec REQUIRED)
FIND_PACKAGE(Leptonica REQUIRED)
FIND_PACKAGE(Tesseract REQUIRED)
if (${TESSERACT_HAS_OEM_LSTM_ONLY})
  add_definitions(-DTESSERACT_HAS_OEM_LSTM_ONLY)
endif()
FIND_PACKAGE(Doxygen REQUIRED dot)
FIND_PACKAGE(Zopfli REQUIRED)

# Get list of header files.

SET(libscantools_HEADERS
  bitVector.h
  compression.h
  HOCRDocument.h
  HOCRTextBox.h
  imageInfo.h
  imageOperations.h
  JBIG2Document.h
  JBIG2Segment.h
  length.h
  paperSize.h
  PDFAWriter.h
  resolution.h
  scantools.h
  TIFFReader.h
)


#
# Generate documentation using doxygen
#

CONFIGURE_FILE(Doxyfile.in Doxyfile)
ADD_CUSTOM_COMMAND(OUTPUT APIdoc DEPENDS ${libscantools_HEADERS} COMMAND doxygen)


#
# Build library
#

CONFIGURE_FILE(scantools-aux.h.in scantools-aux.h)
ADD_LIBRARY(scantools SHARED
  APIdoc
  bitVector.cpp
  compression.cpp
  compression_fax.cpp
  HOCRDocument.cpp
  HOCRDocument_tesseract.cpp
  HOCRTextBox.cpp
  imageInfo.cpp
  imageOperations.cpp
  JBIG2Document.cpp
  JBIG2Document_jbig2dec.cpp
  JBIG2Segment.cpp
  JP2Box.cpp
  length.cpp
  libscantools.qrc
  paperSize.cpp
  PDFAWriter.cpp
  PDFAWriter_PDFObjects.cpp
  resolution.cpp
  scantools.cpp
  TIFFReader.cpp
  )

INCLUDE_DIRECTORIES(${ZOPFLI_INCLUDE_DIR} ${JBIG2DEC_INCLUDE_DIR} ${TESSERACT_INCLUDE_DIR})
INCLUDE_DIRECTORIES(${JBIG2DEC_INCLUDE_DIR} ${TESSERACT_INCLUDE_DIR})
TARGET_COMPILE_DEFINITIONS(scantools PRIVATE JBIG_NO_MEMENTO=1)
TARGET_LINK_LIBRARIES(scantools Qt5::Concurrent Qt5::Gui ${JPEG_LIBRARIES} ${TESSERACT_LIBRARIES} ${JBIG2DEC_LIBRARIES} ${ZOPFLI_LIBRARIES} ${LEPTONICE_LIBRARIES} z)
SET_TARGET_PROPERTIES(scantools PROPERTIES
    SOVERSION ${PROJECT_VERSION_MAJOR}
    VERSION ${PROJECT_VERSION}
    C_VISIBILITY_PRESET hidden
    )
TARGET_PRECOMPILE_HEADERS(scantools PRIVATE
    <QObject>
    <QPointer>
    )


#
# Install things
#

INSTALL(TARGETS scantools
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    )
INSTALL(FILES ${libscantools_HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/scantools)
INSTALL(DIRECTORY ${PROJECT_BINARY_DIR}/src/libscantools/APIdoc/html DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/doc/scantools-devel)


#
# Unit tests and benchmarks
#

# bitVector
ADD_EXECUTABLE(bitVectorTest bitVectorTest.cpp)
TARGET_LINK_LIBRARIES(bitVectorTest scantools Qt5::Test)
ADD_TEST(bitVectorTest bitVectorTest)

# compression
ADD_EXECUTABLE(compressionTest compressionTest.cpp)
TARGET_LINK_LIBRARIES(compressionTest scantools Qt5::Test)
ADD_TEST(compressionTest compressionTest)

# imageInfo
ADD_EXECUTABLE(imageInfoTest imageInfoTest.cpp)
TARGET_LINK_LIBRARIES(imageInfoTest scantools Qt5::Test)
ADD_TEST(imageInfoTest imageInfoTest)

# imageOperations
ADD_EXECUTABLE(imageOperationsTest imageOperationsTest.cpp)
TARGET_LINK_LIBRARIES(imageOperationsTest scantools Qt5::Test)
ADD_TEST(imageOperationsTest imageOperationsTest)

# length
ADD_EXECUTABLE(lengthTest lengthTest.cpp)
TARGET_LINK_LIBRARIES(lengthTest scantools Qt5::Test)
ADD_TEST(lengthTest lengthTest)

# paperSize
ADD_EXECUTABLE(paperSizeTest paperSizeTest.cpp)
TARGET_LINK_LIBRARIES(paperSizeTest scantools Qt5::Test)
ADD_TEST(paperSizeTest paperSizeTest)

# resolution
ADD_EXECUTABLE(resolutionTest resolutionTest.cpp)
TARGET_LINK_LIBRARIES(resolutionTest scantools Qt5::Test)
ADD_TEST(resolutionTest resolutionTest)

# TIFFReader
ADD_EXECUTABLE(TIFFReaderTest TIFFReaderTest.cpp)
TARGET_LINK_LIBRARIES(TIFFReaderTest scantools Qt5::Test)
ADD_TEST(TIFFReaderTest TIFFReaderTest)
