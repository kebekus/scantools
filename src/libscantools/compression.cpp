/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "zlib.h"
#include "zopfli.h"

#include "compression.h"


auto compression::zlibCompress(QByteArray &src, zlibCompressionStrategy strategy) -> QByteArray
{
  // Destination buffer, must be at least (1.01X + 12) bytes as large as source.. we made it 1.1X + 12bytes
  uLongf sizeDataCompressed  = (src.size() * 1.1) + 12;
  QByteArray compressed(sizeDataCompressed, 0);

  
  uLong sourceLen = src.size();

  z_stream stream;
  stream.zalloc = nullptr;
  stream.zfree  = nullptr;
  stream.opaque = nullptr;

  const uInt max = static_cast<uInt>(-1);
  uLong left = sizeDataCompressed;
  
  int err = deflateInit2(&stream, Z_BEST_COMPRESSION, Z_DEFLATED, 15, 8, strategy == filtered ? Z_FILTERED : Z_DEFAULT_STRATEGY);
  if (err != Z_OK) {
    return QByteArray();
  }
  
  stream.next_out  = reinterpret_cast<Bytef*>(compressed.data());
  stream.avail_out = 0;
  stream.next_in   = reinterpret_cast<z_const Bytef *>(src.data());
  stream.avail_in  = 0;
  
  do {
    if (stream.avail_out == 0) {
      stream.avail_out = left > (uLong)max ? max : static_cast<uInt>(left);
      left -= stream.avail_out;
    }
    if (stream.avail_in == 0) {
      stream.avail_in = sourceLen > (uLong)max ? max : (uInt)sourceLen;
      sourceLen -= stream.avail_in;
    }
    err = deflate(&stream, sourceLen ? Z_NO_FLUSH : Z_FINISH);
  } while (err == Z_OK);
  deflateEnd(&stream);
  
  
  if (err != Z_STREAM_END) {
    return QByteArray();
  }
  
  // The variable 'sizeDataCompressed' now contains the size of the compressed data
  // in bytes.
  compressed.truncate(stream.total_out);
  
  return compressed;
}


auto compression::zopfliCompress(const QByteArray &src) -> QByteArray
{
  ZopfliOptions options;
  ZopfliInitOptions(&options);

  unsigned char* out = nullptr;
  size_t outsize = 0;
  ZopfliCompress(&options, ZOPFLI_FORMAT_ZLIB, (const unsigned char *)src.data(), src.size(), &out, &outsize);
  QByteArray result((const char *)out, (int)outsize);
  free(out);
  
  return result;
}


auto compression::zlibUncompress(const QByteArray &src, quint64 _estimatedOutputSize) -> QByteArray
{
  QByteArray result;

  uLongf estimatedResultSize;
  if (_estimatedOutputSize > 0) {
    estimatedResultSize = _estimatedOutputSize;
  } else {
    estimatedResultSize = 2*src.size()+1000;
  }
  
  int z_result = 0;
  do{
    estimatedResultSize = estimatedResultSize*2;
    result.clear();
    result.resize(estimatedResultSize);
    z_result = uncompress((Bytef *)result.data(), (uLongf *)&estimatedResultSize, (const Bytef *)src.data(), src.size());
  }while(z_result == Z_BUF_ERROR);
  
  if (z_result != Z_OK) {
    return QByteArray();
  }
  
  result.truncate(estimatedResultSize);
  return result;
}
