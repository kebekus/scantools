/*
 * Copyright © 2017 - 2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef bitVECTOR
#define bitVECTOR 1


#include <QByteArray>
#include <QString>

/** The bitVector class provides an array of bits, similar to QBitArray

    A bitVector is an array that gives access to individual bits. In this
    respect, the class is very similar to QBitArray. Unlike QBitArray, there are
    convenient and well-defined conversions from bitVectors to QByteArrays with
    guraranteed behaviour.

    The bitVector class is based on QByteArray, and therefore uses implicit
    sharing (copy-on-write) to reduce memory usage and to avoid the needless
    copying of data.  The methods of this class are reentrant, but not thread
    safe.  

    @warning The implementation has been tested for correctness, but has not
    been optimized for speed.
*/

class bitVector
{
 public:
  /** Constructs an empty bit array */
  bitVector();
  
  /** Constructs an array of size bits

      @param numBits Size of the newly created bit vector
      
      The bit are initialized to the value '0'.
  */
  explicit bitVector(int numBits);
  
  /** Constructs a bitVector from a QByteArray 
      
      Constructs an array of 8*data.size() bits. Bit number 0 in the resulting
      bitVector corresponds to the most significant bit in byte 0 of the
      QByteArray. Bit number 1 corresponds to the second most significant bit in
      byte 0 of the QByteArray, etc.

      @param data QByteArray whose data is copied
      
      This operation uses implicit sharing and therefore takes constant time.
  */
  explicit bitVector(QByteArray data);
  
  /** Returns the bit at position index. 
      
      @param index This must be a valid index position in the bit array. In
      other words, index < size(). Invalid values will likely lead to
      segfault
      
      @returns Either 0 or 1
  */
  inline quint8 getBit(int index) const {
    return (content[index/8] >> (7-index%8)) & 0x01;
  }
  
  /** Sets the bit at position index. 
      
      @param index This must be a valid index position in the bit array. In
      other words, index < size(). Invalid values for index will likely
      lead to segfault

      @param bit Bit value, either 0 or 1.
  */
  inline void setBit(int index, quint8 bit) {
    quint32 byteIndex = index/8;
    if (bit) 
      content[byteIndex] = content[byteIndex] |  (0b00000001 << (7-index%8));
    else
      content[byteIndex] = content[byteIndex] & ~(0b00000001 << (7-index%8));
  }

  /** Size of the bitVector in bits

      @return Size of the bitVector, in number of bits
   */
  inline int size() const {
    return _size;
  }

  /** Sets the size of the bitVector array to newSize bits.

      If size is greater than the current size, the bitVector is extended by
      adding the extra bits to the end. The new bits are uninitialized.  If
      newSize is less than the current size, bits are removed from the end.

      @param newSize New size of the vector, in bits
  */
  void resize(int newSize);
  
  /** Simple array of bits, useful for static data
      
      This class provides a miniature array, storing at most 32 bits. It is
      mainly useful for storing arrays of bits statically, as in the following
      example.
      \code
bitVector::miniBitVector terminatingCodesWhite[] = { 
{ 8, 0b00110101}, // Mini vector of size 8, with bits 0, 0, 1, 1, 0, 1, 0, 1
{ 6, 0b000111}    // Mini vector of size 6, with bits 0, 0, 0, 1, 1, 1
};
\endcode
      
  */
  class miniBitVector {
  public:
    /** Number of bits stored, must be smaller than or equal to 32 */
    quint8 numBits;
    
    /** Array of bits, there the least significant bit is the last of the bits stored. */
    quint32 value;
    
    /** Returns the bit at position index. 
	
	@param index This must be a valid index position in the bit array. In
	other words, index < numBits. Invalid values will likely lead to
	segfault
	
	@returns Either 0 or 1
    */
    inline quint8 getBit(int index) const {
      return (value >> (numBits-1-index)) & 0x01;
    }
  };
  
  /** Check if the content of the given miniBitVector is found at a given
      position

      @param mbv miniBitVector whose content is expected to by found at position
      index

      @param index Position where the content of mbv is expected

      @returns True if the content of the given miniBitVector is found a
      position index; otherwise returns false
   */
  bool startsWith(const miniBitVector mbv, int index=0) const;

  /** Appends a miniBitVector to the end of the bitVector

      The bitVector will be resized if necessary.

      @param mbv miniBitVector whose content is appended
  */
  inline void append(const miniBitVector mbv) {
    // If contents array is too small, expand its size
    if (_size+32 > content.size()*8)
      content.resize(2*content.size()+1000);
    
    quint32 value = mbv.value;
    for(int i=0; i<mbv.numBits; i++) {
      bool bit = value & 0x01;
      value = value >> 1;
      setBit(_size+mbv.numBits-1-i, bit);
    }
    
    _size += mbv.numBits;
  }

  /** Converts the bitVector to a QByteArray

      Stores the bitVector in an array of (size()+7)/8 bytes. Bit number 0 in
      the bitVector corresponds to the most significant bit in byte 0 of the
      QByteArray. Bit number 1 corresponds to the second most significant bit in
      byte 0 of the QByteArray, etc.  Unused bits in the last byte of the
      resulting array are set to zero.

      @return QByteArray containing the bitvector data
  */
  operator QByteArray();
  
  /** Convert bitVector to human-readable string

      @return QString describing the bitVector
  */
  operator QString() const;
  
  /** Convenience method for read access to a bit in a C-array of bytes
      
      @param ptr This is a pointer to an array by bytes (a.k.a. quint8). 
      
      @param index Number of the bit that is retrieved from the array. Bit
      number 0 is the most significant bit in byte 0 of the array. Bit number 1
      is the second most significant bit in byte 0 of the array, etc.  Indices
      that point out of the array boundes will likely lead to segfault.
      
      @returns Either 0 or 1
  */
  static inline quint8 getBit(const quint8 *ptr, int index) {
    return (ptr[index/8] >> (7-index%8)) & 0x01;
  }

  /** Convenience method for write access to a bit in a C-array of bytes

      @param ptr This is a pointer to an array by bytes (a.k.a. quint8). 
      
      @param index This must be a valid index position in the bit array. Bit
      number 0 is the most significant bit in byte 0 of the array. Bit number 1
      is the second most significant bit in byte 0 of the array, etc.  Indices
      that point out of the array boundes will likely lead to segfault.
      
      @param bit Either 0 or 1
  */
  static inline void setBit(quint8 *ptr, int index, quint8 bit) {
    quint32 byteIndex = index/8;
    if (bit) 
      ptr[byteIndex] = ptr[byteIndex] |  (0b00000001 << (7-index%8));
    else
      ptr[byteIndex] = ptr[byteIndex] & ~(0b00000001 << (7-index%8));
  }
  
  /** Convenience method for write access to a continuous block of bits in a
      C-array of bytes
      
      @param ptr This is a pointer to an array by bytes (a.k.a. quint8). 
      
      @param startIndex This must be a valid index position in the bit
      array. Bit number 0 is the most significant bit in byte 0 of the
      array. Bit number 1 is the second most significant bit in byte 0 of the
      array, etc.  Indices that point out of the array boundes will likely lead
      to segfault.

      @param numBits Number of bits to be written. The array must be at least of
      size startIndex+numBits, or else a segfault will likely occur.
      
      @param bit Either 0 or 1
  */
  static inline void fill(quint8 *ptr, int startIndex, int numBits, quint8 bit) {
    quint32 endIndex = startIndex+numBits;
    quint32 index=startIndex;
    while(index < endIndex) {
      
      if (index%8 == 0) {
	uintptr_t currentBytePtr = (uintptr_t)(ptr+index/8);
	
	if ((currentBytePtr%8 == 0) && (index+64<endIndex)) {
	  *((quint64 *)currentBytePtr) = (bit == 0) ? 0x0000000000000000 : 0xFFFFFFFFFFFFFFFF;
	  index += 64;
	  continue;
	}
	
	if ((currentBytePtr%4 == 0) && (index+32<endIndex)) {
	  *((quint32 *)currentBytePtr) = (bit == 0) ? 0x00000000 : 0xFFFFFFFF;
	  index += 32;
	  continue;
	}
	
	if ((currentBytePtr%2 == 0) && (index+16<endIndex)) {
	  // cppcheck-suppress unreadVariable
	  *((quint16 *)currentBytePtr) = (bit == 0) ? 0x0000 : 0xFFFF;
	  index += 16;
	  continue;
	}
	
	if (index+8<endIndex) {
	  ptr[index/8] = (bit == 0) ? 0x00 : 0xFF;
	  index += 8;
	  continue;
	}
      } // endif: index is on byte boundary
      
      bitVector::setBit(ptr, index, bit);
      index++;
    }
  }

  /** Searches for end of a run of consecutive bits with same value

      @param ptr Pointer to the data whose content is searched

      @param startIndex Index of the first bit to be looked at

      @param endIndex Index of the last bit to be looked at

      @param runValue Value of the bits in the run
      
      @returns Index of the first bit in the interval [startIndex : endIndex-1]
      that is not of value 'runValue'. If no such bit has been found, 'endIndex'
      is returned.
  */
  static inline int findEndOfRun(const quint8 *ptr, int startIndex, int endIndex, quint8 runValue) {
    int index=startIndex;
    
    while(index < endIndex) {

      if (index%8 == 0) {
	uintptr_t currentBytePtr = (uintptr_t)(ptr+index/8);

	// Handle 8-byte words efficiently
	if ((currentBytePtr%8 == 0) && (endIndex-index >= 64)) {
	  if (runValue == 0) {
	    if ( *((quint64 *)currentBytePtr) == 0) {
	      index += 64;
	      continue;
	    }
	  } else {
	    if ( *((quint64 *)currentBytePtr) == 0xFFFFFFFFFFFFFFFF) {
	      index += 64;
	      continue;
	    }
	  }
	}
	
	// Handle 4-byte words efficiently
	if ((currentBytePtr%4 == 0) && (endIndex-index >= 32)) {
	  if (runValue == 0) {
	    if ( *((quint32 *)currentBytePtr) == 0) {
	      index += 32;
	      continue;
	    }
	  } else {
	    if ( *((quint32 *)currentBytePtr) == 0xFFFFFFFF) {
	      index += 32;
	      continue;
	    }
	  }
	}
      
	// Handle 2-byte words efficiently
	if ((currentBytePtr%2 == 0) && (endIndex-index >= 16)) {
	  if (runValue == 0) {
	    if ( *((quint16 *)currentBytePtr) == 0) {
	      index += 16;
	    continue;
	    }
	  } else {
	    if ( *((quint16 *)currentBytePtr) == 0xFFFF) {
	      index += 16;
	      continue;
	    }
	  }
	}
      
	// Handle 1-byte words efficiently
	if (endIndex-index >= 8) {
	  if (runValue == 0) {
	    if (ptr[index/8] == 0) {
	      index += 8;
	      continue;
	    }
	  } else {
	    if (ptr[index/8] == 0xFF) {
	      index += 8;
	      continue;
	    }
	  }
	}
      } // endif: index is on byte boundary
      
      if (bitVector::getBit(ptr, index) != runValue)
	return index;
      index++;
    }
    
    return endIndex;
  }
  
 private:
  int _size;
  
  QByteArray content;
};

#endif
