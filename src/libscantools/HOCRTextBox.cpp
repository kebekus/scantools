/*
 * Copyright © 2016 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "HOCRTextBox.h"
#include "scantools.h"

#include <QDebug>
#include <QTextCodec>
#include <QtGlobal>
#include <cmath>


HOCRTextBox::HOCRTextBox() :
    _angle(0.0), _confidence(-1), _fontSize(0.0)
{
    ;
};


auto HOCRTextBox::hasText() const -> bool
{
    // Return true if this box has non-trivial text
    if (!_text.trimmed().isEmpty()) {
        return true;
    }

    // Return true if any of the subboxes has non-trivial text
    for(const auto & _subBoxe : _subBoxes) {
        if (_subBoxe.hasText()) {
            return true;
        }
    }

    // Otherwise, only trivial text found, so return false.
    return false;
}


auto HOCRTextBox::classType() const -> QString
{
    if (_class.startsWith("ocr_") || _class.startsWith("ocrx_")) {
        return _class;
    }
    return QString();
}


HOCRTextBox::HOCRTextBox(QXmlStreamReader &xml, QSet<QString> &warnings, HOCRTextBox *parent) : HOCRTextBox()
{
    // Inherit some attributes from parent, if there is a parent
    if (parent != nullptr) {
        _angle = parent->_angle;
        _baselinePolynomial = parent->_baselinePolynomial;
        _baselineReferencePoint = parent->_baselineReferencePoint;
        _direction = parent->_direction;
        _fontSize = parent->_fontSize;
        _language = parent->_language;
    }

    // Read attributes from the XML file, and interpret the attributes
    _attributes = xml.attributes();
    interpretAttributes(warnings, xml.lineNumber(), xml.columnNumber());
    // Read language name
    QString tmp = _attributes.value("lang").toString().simplified();
    if (!tmp.isEmpty()) {
        _language = tmp;
    }
    // Read text flow direction
    tmp = _attributes.value("dir").toString().simplified();
    if (!tmp.isEmpty()) {
        _direction = tmp;
    }

    // Adjust angle. If no _angle has been set, yet a non-constant
    // baselinepolynomial exists, then use this polynomial to reconstruct the
    // angle.
    if (qFuzzyCompare(_angle+1.0, 1.0) && // Look at Qt documentation why adding 1.0 is the right thing to do
        (!_baselinePolynomial.empty()) && (!_boundingBox.isEmpty())) {
        qreal x = (_boundingBox.right()+_boundingBox.left())/2.0 - _baselineReferencePoint.x();
        qreal D = 0.0;

        // Evaluate derivative of polynomial
        for(int i=0; i < _baselinePolynomial.size()-1; i++) {
            D = D*x+_baselinePolynomial[i];
        }

        if (!qFuzzyCompare(D+1.0, 1.0)) { // Look at Qt documentation why adding 1.0 is the right thing to do
            _angle = -atan(D)*(360.0/(2.0*M_PI));
        }
    }


    // Continue to read XML until we run into the matching end element
    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isStartElement()) {
            HOCRTextBox child(xml, warnings, this);
            if (child.boundingBox().isEmpty()) {
                _text += child._text;
                _subBoxes += child._subBoxes;
            } else {
                _subBoxes.append(child);
            }
            continue;
        }

        if (xml.isCharacters()) {
            _text += xml.text();
            continue;
        }

        if (xml.isEndElement()) {
            break;
        }
    }


    // Now optimize a little bit.
    if (_text.simplified().isEmpty()) {
        _text = QString();
    }
}


void HOCRTextBox::interpretAttributes(QSet<QString> &warnings, qint64 line, qint64 column)
{
    // Check class
    _class = _attributes.value("class").toString().simplified();

    // Check if current element specifies OCR data, and if so, interpret the OCR
    // attributes
    if (!_class.startsWith("ocr_") && !_class.startsWith("ocrx_")) {
        return;
    }

    /*
     * Extract all data hidden in the 'title' attribute
     */
    QVector<QStringRef> OCRAttributes = _attributes.value("title").split(";");
    bool specifiesBaselinePolynomial = false;

    // Go through the list
    for(auto & i : OCRAttributes) {
        QString OCRAttribute = i.toString().simplified();

        // We have no use for ascenders, so ignore them
        if (OCRAttribute.startsWith("x_ascenders")) {
            continue;
        }

        // Does the attribute specify a baseline?
        if (OCRAttribute.startsWith("baseline")) {
            _baselinePolynomial = getFloats(OCRAttribute);
            specifiesBaselinePolynomial = true;
            continue;
        }

        // Does the attribute specify a bounding box?
        if (OCRAttribute.startsWith("bbox")) {
            QVector<int> numbers = getIntegers(OCRAttribute);
            if (numbers.size() == 4) {
                _boundingBox = QRect(numbers[0], numbers[1], numbers[2]-numbers[0], numbers[3]-numbers[1]);
            } else {
                warnings << QString("Line %1, Column %2. Error interpreting OCRAttribute: '%3'").arg(line).arg(column).arg(OCRAttribute);
            }
            continue;
        }

        // We have no use for image md5 sums, so ignore them
        if (OCRAttribute.startsWith("imagemd5")) {
            continue;
        }

        // Does the attribute specify an image name?
        if (OCRAttribute.startsWith("image")) {
            _imageName = OCRAttribute.section(" ", 1).replace("\"", "");
            continue;
        }

        // We have no use for logical page numbers, so ignore them
        if (OCRAttribute.startsWith("lpageno")) {
            continue;
        }

        // We have no use for physical page numbers, so ignore them
        if (OCRAttribute.startsWith("ppageno")) {
            continue;
        }

        // Text angle
        if (OCRAttribute.startsWith("textangle")) {
            QVector<qreal> numbers = getFloats(OCRAttribute);
            if (numbers.size() == 1) {
                _angle = numbers[0];
            } else {
                warnings << QString("Line %1, Column %2. Error interpreting textangle: '%3'").arg(line).arg(column).arg(OCRAttribute);
            }
            continue;
        }

        // We have no use for descenders, so ignore them
        if (OCRAttribute.startsWith("x_descenders")) {
            continue;
        }

        // Does the attribute specify a font size?
        if (OCRAttribute.startsWith("x_size")) {
            QVector<qreal> numbers = getFloats(OCRAttribute);
            if (numbers.size() == 1) {
                _fontSize = numbers[0];
            } else {
                warnings << QString("Line %1, Column %2. Error interpreting x_size: '%3'").arg(line).arg(column).arg(OCRAttribute);
            }
            continue;
        }

        // Does the attribute specify a confidence level?
        if (OCRAttribute.startsWith("x_wconf")) {
            QVector<qreal> numbers = getFloats(OCRAttribute);
            if (numbers.size() == 1) {
                _confidence = qRound(numbers[0]+0.5);
            } else {
                warnings << QString("Line %1, Column %2. Error interpreting x_wconf: '%3'").arg(line).arg(column).arg(OCRAttribute);
            }
            continue;
        }

        // Ignore a few other attributes that we do not care about
        if (OCRAttribute.startsWith("scan_res"))
        {
            continue;
        }

        // Warn about attributes that were not recognized
        warnings << QString("Line %1, Column %2. Unrecognized OCR attribute: '%3'. "
                            "Please report this, so we can extend scantools appropriately. "
                            "Please include a sample file in your report.").arg(line).arg(column).arg(OCRAttribute);
    }

    if (specifiesBaselinePolynomial) {
        _baselineReferencePoint = _boundingBox.bottomLeft();
    }
}


auto HOCRTextBox::getRenderingHints(const QFont &font) const -> HOCRTextBox::renderingHints
{
    // Construct result and set default values
    renderingHints result;
    result.fontSize = 12;
    result.referencePoint = _boundingBox.bottomLeft();
    result.horizontalStretchFactor = 1.0;

    // If the text is empty, or if the boundingBox is empty, there is nothing to
    // be rendered anyways, so we'll just return the default values.
    if (_boundingBox.isEmpty() || _text.isEmpty()) {
        return result;
    }


    //
    // PREPARATIONS
    //

    // Compute sin and cosine
    qreal radianangle = _angle*(2.0*M_PI/360.0);
    qreal _sin = sin(radianangle);
    qreal _cos = cos(radianangle);
    qreal _sinAbs = qAbs(_sin);
    qreal _cosAbs = qAbs(_cos);

    // We will later use the text box a lot (i.e. the box filled by the text if
    // rendered using the given font).  To compute the box, get a metric for the
    // font in size 100.
    QFont myFont(font);
    myFont.setPixelSize(100);
    QFontMetrics metrics(myFont);
    QRect textBox = metrics.tightBoundingRect(_text);
    qreal textBoxWidth  = textBox.width();
    qreal textBoxHeight = textBox.height();
    qreal textBoxBottom = textBox.bottom();
    // If the size of the text box is zero, something went terribly wrong. Just
    // return with our default values in that case.
    if (textBox.isEmpty()) {
        return result;
    }

    // We will also need the size of smallest rectangle that contains the text box
    // when rotated according to _angle.
  qreal rotatedTextWidth  = _cosAbs*textBoxWidth + _sinAbs*textBoxHeight;
    qreal rotatedTextHeight = _sinAbs*textBoxWidth + _cosAbs*textBoxHeight;

    // If the size of the rotated text box is zero, something went terribly
    // wrong. Just return with our default values in that case.
    if (qFuzzyCompare(rotatedTextWidth*rotatedTextHeight+1.0, 1.0)) { // Look at Qt documentation why this is the right comparison
        return result;
    }


    //
    // FIND ESTIMATE FOR FONT SIZE
    //

    // If a font size is specified in the HOCR file, things are simple. Just take
    // that one.
    if (_fontSize > 1) {
        result.fontSize = qRound(_fontSize);
    } else {
        // Otherwise, we chose the biggest font size so that the rotated text box
        // stays inside the bounding box.  We made sure above that the following
        // computation will never lead to a division by zero.
        result.fontSize = qRound( 100.0*qMin(_boundingBox.width()/rotatedTextWidth, _boundingBox.height()/rotatedTextHeight));
    }

    // UPDATE TEXT BOX
    qreal scale        = result.fontSize/100.0;
    textBoxWidth      *= scale;
    textBoxHeight     *= scale;
    textBoxBottom     *= scale;


    //
    // FIND OPTIMAL HORIZONTAL STRETCH
    //

    // If the textBox is nearly horizontal or vertical, we compute a stretch
    // factor so that the text fills the box entirely. Otherwise, no meaningful
    // stretch factor can be computed, and we just stick to our default value of
    // 1.0.

    // If textBox is nearly horizontal…
    if (_sinAbs < 0.1) {
        result.horizontalStretchFactor = _boundingBox.width() / textBoxWidth;
    }
    // If textBox is nearly vertical, things are simple
    if (_cosAbs < 0.1) {
        result.horizontalStretchFactor = _boundingBox.height() / textBoxWidth;
    }

    // UPDATE TEXT BOX
    textBoxWidth     *= result.horizontalStretchFactor;


    //
    // FIND OPTIMAL REFERENCE POINT
    //

    // Compute reference point just using the bounding box
    qreal boundingBoxMidPointX = _boundingBox.left() + _boundingBox.width()/2.0;
    qreal boundingBoxMidPointY = _boundingBox.top()  + _boundingBox.height()/2.0;
    qreal refX = qRound( boundingBoxMidPointX + (-_cos*textBoxWidth + _sin*textBoxHeight)/2.0 - _sin*textBoxBottom );
    qreal refY = qRound( boundingBoxMidPointY + ( _sin*textBoxWidth + _cos*textBoxHeight)/2.0 - _cos*textBoxBottom );

    // If a baselinePolynomial exists, use that to improve the estimate for refY…
    if ((!_baselinePolynomial.empty()) && (!_boundingBox.isEmpty())) {
        qreal x = _boundingBox.left() - _baselineReferencePoint.x();
        qreal y = 0.0;

        // Evaluate polynomial
        for(double i : _baselinePolynomial) {
            y = y*x+i;
        }

        refY = _baselineReferencePoint.y() + qRound(y);
    }
    result.referencePoint = QPoint(refX, refY);


    // DONE!
    return result;
}


void HOCRTextBox::render(QPainter &painter) const
{
    // Paranoid safety checks
    if (!painter.isActive()) {
        qWarning() << "HOCRTextBox::render called on inactive painter.";
        return;
    }

    // Draw content of this textBox, but only if we do have content and if a
    // bounding box exists
    if (!_boundingBox.isEmpty() && !_text.isEmpty()) {
        // Adjust font size, but only if necessary
        QFont font = painter.font();
        renderingHints hints = getRenderingHints(font);
        int sFontSize = hints.fontSize;
        if (font.pixelSize() != sFontSize) {
            font.setPixelSize(sFontSize);
            painter.setFont(font);
        }

        // Draw the text.
        painter.setWorldTransform(QTransform(hints.horizontalStretchFactor, 0.0, 0.0, 1.0, hints.referencePoint.x(), hints.referencePoint.y()));
        painter.rotate(-_angle);
        painter.drawText(0,0,_text);
        painter.resetTransform();
    }

    // Now render all subboxes that we might have.
    for(const auto & _subBoxe : _subBoxes) {
        _subBoxe.render(painter);
    }
}


auto HOCRTextBox::toImage(const QFont &overrideFont, QImage::Format format) const -> QImage
{
    // If we don't have a bounding box, return empty image
    if (_boundingBox.isEmpty()) {
        return QImage();
    }

    // Generate image
    QImage image(_boundingBox.width(), _boundingBox.height(), format);
    image.setText("creator", QString("scantools %1").arg(scantools::versionString));
    image.fill(Qt::white);

    // Generate painter
    QPainter painter(&image);
    painter.setFont(overrideFont);

    // Paint
    render(painter);

    // Return
    return image;
}


auto HOCRTextBox::toNumber(qreal x) -> QByteArray
{
    QString n = QString::number(x, 'f', 4);
    int i = n.length()-1;

    // Remove all trailing zeroes
    while(n[i] == '0') {
        i--;
    }

    // Remove trailing dot
    if (n[i] == '.') {
        i--;
    }

    // return result
    return n.left(i+1).toLatin1();
}


auto HOCRTextBox::toRawPDFContentStream(const QFont &font, resolution xRes, resolution yRes, length deltaX, length deltaY) const -> QByteArray
{
    // Paranoid safety checks
    if (!xRes.isValid() || !yRes.isValid()) {
        return QByteArray();
    }

    // Get codec
    QTextCodec *codec = QTextCodec::codecForName("Windows-1252");
    if (codec == nullptr) {
        qFatal("Fatal error. Codec 'Windows-1252' not found");
    }

    // Go!
    qreal currentFontSize = -1.0;
    return toRawPDFContentStream(font, xRes, yRes, deltaX, deltaY, boundingBox().height(), currentFontSize, codec);
}


auto HOCRTextBox::toRawPDFContentStream(const QFont &overrideFont, resolution xRes, resolution yRes, length deltaX, length deltaY, quint16 height, qreal &currentFontSize, QTextCodec *codec) const -> QByteArray
{
    QByteArray result;

    // Render the text for all the sub-boxed involved.
    foreach(auto subBox, _subBoxes)
        result += subBox.toRawPDFContentStream(overrideFont, xRes, yRes, deltaX, deltaY, height, currentFontSize, codec);

    // If we have no text ourselves, the show ends here.
    if (_text.isEmpty()) {
        return result;
    }

    // Specify font and font size, if necessary
    renderingHints hints = getRenderingHints(overrideFont);
    int sFontSize = hints.fontSize;
    auto fontSize = sFontSize*72.0/xRes.get(resolution::dpi);
    if (fontSize != currentFontSize) {
        currentFontSize = fontSize;
        QByteArray locRes1 = "/F1 %s Tf ";
        locRes1.replace("%s", toNumber(fontSize));
        result += locRes1;
    }

    // Get rotation and reference point
    QPoint referencePt = hints.referencePoint;

    // Compute sin and cosine
    qreal radianangle = _angle*(2.0*M_PI/360.0);
    qreal _sin = sin(radianangle);
    qreal _cos = cos(radianangle);

    // Set rotation and reference point
    QByteArray locRes3 = "%a %b %c %d %x %y Tm ";
    locRes3.replace("%a", toNumber( _cos*hints.horizontalStretchFactor));
    locRes3.replace("%b", toNumber( _sin));
    locRes3.replace("%c", toNumber(-_sin*hints.horizontalStretchFactor));
    locRes3.replace("%d", toNumber( _cos));
    locRes3.replace("%x", toNumber( deltaX.get(length::pt)+referencePt.x()*72.0/xRes.get(resolution::dpi)));
    locRes3.replace("%y", toNumber( deltaY.get(length::pt)+(height-referencePt.y())*72.0/yRes.get(resolution::dpi)));
    result += locRes3;

    // OCR systems like tesseract recognise many ligatures and digraphs that
    // cannot be encoded in the Windows-1252 encoding. To improve our output, we
    // replace those.
    QString text = _text;

    // Digraphs
    text.replace("Ǳ", "DZ");
    text.replace("ǲ", "Dz");
    text.replace("ǳ", "dz");
    text.replace("Ǆ", "DŽ");
    text.replace("ǅ", "Dž");
    text.replace("ǆ", "dž");
    text.replace("Ĳ", "IJ");
    text.replace("ĳ", "ij");
    text.replace("Ǉ", "LJ");
    text.replace("ǈ", "Lj");
    text.replace("ǉ", "lj");
    text.replace("Ǌ", "NJ");
    text.replace("ǋ", "Nj");
    text.replace("ǌ", "nj");

    // Typographical ligatures
    text.replace("Ꜳ", "AA");
    text.replace("ꜳ", "aa");
    text.replace("Ꜵ", "AO");
    text.replace("ꜵ", "ao");
    text.replace("Ꜷ", "AU");
    text.replace("ꜷ", "au");
    text.replace("Ꜹ", "AV");
    text.replace("ꜹ", "av");
    text.replace("Ꜻ", "AV");
    text.replace("ꜻ", "av");
    text.replace("Ꜽ", "AY");
    text.replace("ꜽ", "ay");
    text.replace("🙰", "et");
    text.replace("ﬀ", "ff");
    text.replace("ﬃ", "ffi");
    text.replace("ﬄ", "ffl");
    text.replace("ﬁ", "fi");
    text.replace("ﬂ", "fl");
    text.replace("Ꝏ", "OO");
    text.replace("ꝏ", "oo");
    text.replace("ſs", "ß");
    text.replace("ſz", "ß");
    text.replace("ẞ", "ß");
    text.replace("ﬆ", "st");
    text.replace("Ꜩ", "TZ");
    text.replace("ꜩ", "tz");
    text.replace("ᵫ", "ue");

    // Other symbols
    text.replace("—", "-");

    // Now show text.
    QByteArray locRes = "<%t> Tj ";
    locRes.replace("%t", codec->fromUnicode(text).toHex());
    result += locRes;

    return result;
}


auto HOCRTextBox::toText() const -> QString
{
    // Generate text
    QStringList texts;
    texts.reserve(_subBoxes.size());
    for(const auto & _subBoxe : _subBoxes) {
        texts << _subBoxe.toText();
    }
    texts << _text.simplified();
    QString text = texts.join(" ");

    // Add line break
    QStringList typesWithLineBreak;
    typesWithLineBreak << "ocr_page" << "ocr_carea" << "ocr_par" << "ocr_line";
    if (typesWithLineBreak.contains(classType())) {
        text += "\n";
    }

    // Return text
    return text;
}


auto HOCRTextBox::estimateFit(const QFont &font) const -> qint64
{
    qint64 result = 0;

    // Go through all the subboxes that we might have and colled the estimates
    // that we have there
    for(const auto & _subBoxe : _subBoxes) {
        result += _subBoxe.estimateFit(font);
    }

    // Compute fit for the content of this textBox, but only if we do have
    // content, if a bounding box exists and if the text is nearly horizontal
    if (_boundingBox.isEmpty() || _text.isEmpty() || (fabs(_angle) > 5) ) {
        return result;
    }

    // Adjust font size,  if necessary
    renderingHints hints = getRenderingHints(font);
    int sFontSize = hints.fontSize;
    QFont myFont(font);
    if (myFont.pixelSize() != sFontSize) {
        myFont.setPixelSize(sFontSize);
    }
    QFontMetrics metrics(myFont);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 11, 0))
    qint64 delta = _boundingBox.width()-metrics.horizontalAdvance(_text);
#else
    qint64 delta = _boundingBox.width()-metrics.width(_text);
#endif

    result += delta*delta;

    return result;
}


auto HOCRTextBox::getIntegers(const QString& spec) const -> QVector<int>
{
    QStringList specs = spec.split(" ");
    if (specs.size() < 2) {
        return QVector<int>(0);
    }

    QVector<int> result(specs.size()-1);
    for(int i=1; i<specs.size(); i++) {
        result[i-1] = specs[i].toInt();
    }
    return result;
}


auto HOCRTextBox::getFloats(const QString& spec) const -> QVector<qreal>
{
    QStringList specs = spec.split(" ");
    if (specs.size() < 2) {
        return QVector<qreal>(0);
    }

    QVector<qreal> result(specs.size()-1);
    for(int i=1; i<specs.size(); i++) {
        result[i-1] = specs[i].toDouble();
    }
    return result;
}


auto HOCRTextBox::suggestFont() const -> QFont
{
    // List of fonts to try
    QStringList fontNames;
    fontNames << "Helvetica" << "Times" << "Courier";

    // Try every font from the list
    QVector<qint64> fits;
  foreach(auto fontName, fontNames) {
        QFont font(fontName);
        fits << estimateFit(font);
    }

    // Find best fitting font
    int bestFontIndex = 0;
    int bestFontFit = fits[0];
    for(int i=1; i<fits.size(); i++) {
        if (fits[i] < bestFontFit) {
            bestFontIndex = i;
            bestFontFit = fits[i];
        }
    }

    // Return result
    return QFont(fontNames[bestFontIndex]);
}
