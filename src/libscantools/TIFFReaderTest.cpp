/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QProcess>
#include <QTemporaryDir>

#include "TIFFReader.h"
#include "TIFFReaderTest.h"

QTEST_MAIN(TIFFReaderTest)


void TIFFReaderTest::imageReading_data()
{
    QTest::addColumn<QString>("path");

    QDir dir(QFINDTESTDATA("testData/images/TIFF"));
    QFileInfoList files = dir.entryInfoList( QStringList() << "*.tiff" << "*.tif");
    foreach(const QFileInfo &file, files) {
        QTest::newRow(file.absoluteFilePath().toLocal8Bit().constData()) << file.absoluteFilePath();
    }
}


void TIFFReaderTest::imageReading()
{
    QFETCH(QString, path);

    TIFFReader doc(path);
    QImage imageReadByMe = doc[0].convertToFormat(QImage::Format_RGB888);
    QImage imageReadByQt = QImage(path).convertToFormat(QImage::Format_RGB888);

    if (imageReadByQt.isNull()) {
        return;
    }

    QCOMPARE(imageReadByMe, imageReadByQt);
}


void TIFFReaderTest::resolution_data()
{
    QTest::addColumn<QString>("path");

    QDir dir(QFINDTESTDATA("testData/images/TIFF"));
    QFileInfoList files = dir.entryInfoList( QStringList() << "*.tiff" << "*.tif");
    foreach(const QFileInfo &file, files) {
        QTest::newRow(file.absoluteFilePath().toLocal8Bit().constData()) << file.absoluteFilePath();
    }
}


void TIFFReaderTest::resolution()
{
    QFETCH(QString, path);

    TIFFReader doc(path);
    QImage imageReadByMe = doc[0];
    QImage imageReadByQt = QImage(path);

    if (imageReadByQt.isNull()) {
        return;
    }

    QCOMPARE(imageReadByMe.dotsPerMeterX(), imageReadByQt.dotsPerMeterX());
    QCOMPARE(imageReadByMe.dotsPerMeterY(), imageReadByQt.dotsPerMeterY());
}


void TIFFReaderTest::multiPage_data()
{
    QTest::addColumn<QString>("path");

    QDir dir(QFINDTESTDATA("testData/images/TIFF"));
    QFileInfoList files = dir.entryInfoList( QStringList() << "*.tiff" << "*.tif");
    foreach(const QFileInfo &file, files) {
        QTest::newRow(file.absoluteFilePath().toLocal8Bit().constData()) << file.absoluteFilePath();
    }
}


void TIFFReaderTest::multiPage()
{
    QFETCH(QString, path);

    // Setup a temporary directory
    QTemporaryDir tempDir;
    tempDir.path();

    // Split TIFF File
    QProcess tiffsplit;
    tiffsplit.setWorkingDirectory(tempDir.path());
    tiffsplit.start("tiffsplit", QStringList() << path);
    tiffsplit.waitForStarted();
    tiffsplit.waitForFinished();
    Q_ASSERT(tiffsplit.exitStatus() == QProcess::NormalExit);
    Q_ASSERT(tiffsplit.exitCode() == 0);

    // Read Images
    QVector<QImage> referenceImages;
    QDir dir(tempDir.path());
    QFileInfoList files = dir.entryInfoList( QStringList() << "*.tif");
    foreach(const QFileInfo &file, files) {
        referenceImages << QImage(file.absoluteFilePath());
    }

    // Compare images to those obtained by the TIFFReader class
    TIFFReader doc(path);
    QCOMPARE((int)doc.size(), referenceImages.size());
    for(quint32 i=0; i<doc.size(); i++) {
        QCOMPARE(doc[i], referenceImages[i]);
    }
}
