/*
 * Copyright © 2018 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMutexLocker>
#include <QtConcurrent>
#include <utility> 
#include "PDFaddOCR.h"

#include "HOCRDocument.h"


PDFaddOCR::PDFaddOCR(const QString& PDFFileName, QStringList langs) :
    languages(std::move(langs)),
    renderingResolution(300.0, resolution::dpi),
    modifier(PDFFileName),
    renderer(PDFFileName)
{

}


PDFaddOCR::~PDFaddOCR()
= default;


int PDFaddOCR::addOCRtoPage(int pageNumber)
{
    // Render page image
    QImage image = renderer.render(pageNumber, renderingResolution);
    if (image.isNull()) {
        return -1;
    }

    // Do not add a text layer to a page that already contains text.
    if (!image.text("Text").simplified().isEmpty()) {
        return -1;
    }

    // Run tesseract OCR
    HOCRDocument ocrData = HOCRDocument(image, languages);
    image = QImage(); // image is no longer used, free memory
    if (ocrData.hasError()) {
        return -1;
    }
    // If no text, then there is nothing to do for this page…
    if (!ocrData.hasText())
        return 0;

    // Generate text layer
    modifier.addTextLayer(ocrData.takeFirstPage(),  pageNumber,  renderingResolution);
    if (!modifier.errors.isEmpty()) {
        //    printMessage(QString("Error generating text overlay for page %1. ").arg(pageNumber+1) + modifier.errors.join(" "));
        return -1;
    }

    return 0;
}


void PDFaddOCR::progressInt(int jobs)
{
    qreal denominator = watcher.progressMaximum() - watcher.progressMinimum();
    if (denominator != 0.0)
        emit progress(jobs/denominator);
}


void PDFaddOCR::addOCR()
{
    pageNumbers.resize(renderer.numPages);
    for(int pageNumber=0; pageNumber < renderer.numPages; pageNumber++)
        pageNumbers[pageNumber] = pageNumber;

    QObject::connect(&watcher, SIGNAL(progressValueChanged(int)), this, SLOT(progressInt(int)));
    QObject::connect(&watcher, SIGNAL(finished()), this, SIGNAL(finished()));
    QFuture<void> future = QtConcurrent::map(pageNumbers, std::bind(&PDFaddOCR::addOCRtoPage, this, std::placeholders::_1));
    watcher.setFuture(future);

    if (watcher.isFinished())
        emit finished();
}
