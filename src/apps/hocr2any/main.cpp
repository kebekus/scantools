/*
 * Copyright © 2016 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "HOCRDocument.h"
#include "resolution.h"
#include "scantools.h"

#include <QGuiApplication>
#include <QImageWriter>
#include <QtConcurrent>
#include <cstdio>
#include <termios.h>
#include <unistd.h>



/* Sleep until a key is pressed and return value. Inspired from
   http://michael.dipperstein.com/keypress.html */

int keypress()
{
    // Save terminal state
    struct termios savedState{};
    if (-1 == tcgetattr(STDIN_FILENO, &savedState))
        return EOF;

    // Disable canonical input and disable echo.  Set minimal input to 1.
    struct termios newState = savedState;
    newState.c_lflag &= ~ICANON;
    newState.c_cc[VMIN] = 1;
    if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &newState))
        return EOF;

    // Wait until we get a keypress
    int c = getchar();

    // Restore the saved terminal state
    if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &savedState))
        return EOF;

    return c;
}


QString wordwrap(const QString& in, quint16 length=60)
{
    QStringList words = in.simplified().split(QRegExp("\\s+"));
    QString word;

    QString out;
    quint32 lineStart = 0;
    foreach(word, words) {
        if (out.size()+1+word.size()-lineStart > length) {
            lineStart = out.size()+1;
            out = out.isEmpty() ? word : out + '\n' + word;
        } else
            out = out.isEmpty() ? word : out + ' ' + word;
    }

    return out;
}


// Trivial helper function that takes a file name, and creates an HOCRDocument.
// Special case: the file name "-" means: read from stdin.

HOCRDocument loadHOCRDocument(const QString &arg)
{
    if (arg == "-") {
        QFile inFile;
        inFile.open(stdin, QIODevice::ReadOnly);
        return HOCRDocument(&inFile);
    }

    return HOCRDocument(arg);
}


// Trivial helper function that takes a pair of a QImage and a filename saves
// it.  Returns 'false' on success.

bool saveImage(const QPair<QImage, QString> &image_n_filename)
{
    return !image_n_filename.first.save(image_n_filename.second);
}


// Main

int main(int argc, char *argv[])
{
    // Although we need to construct a GUI application (because we use fonts…), we
    // need not X server or Wayland platform.  So, it seems best to choose Qt's
    // 'offscreen' platform plugin before we construct the QGuiApplication --
    // otherwise our program could fail in a headless setting because of lack of
    // an X server.
    setenv("QT_QPA_PLATFORM", "offscreen", 1);
    setenv("XDG_SESSION_TYPE", "", 1);

    // Set up application
    QGuiApplication app(argc, argv);
    QCoreApplication::setOrganizationName("Albert-Ludwigs-Universität Freiburg");
    QCoreApplication::setOrganizationDomain("uni-freiburg.de");
    QCoreApplication::setApplicationName("scantools");
    QCoreApplication::setApplicationVersion(scantools::versionString);

    // Get list of supported image formats
    QList<QByteArray> imageFormatEndingsRaw = QImageWriter::supportedImageFormats();
    QStringList imageFormatEndings;
    foreach(const QString &imageFormatEnding, imageFormatEndingsRaw)
        imageFormatEndings << imageFormatEnding.toUpper();


    /*
   * Set up command line parsing
   */

    QCommandLineParser parser;
    parser.setApplicationDescription("Convert HOCR to PDF, to a sequence of images or to a text file.");
    parser.addHelpOption();
    parser.addVersionOption();

    // Specify positional arguments
    parser.addPositionalArgument("file1.hocr file2.hocr ...", "Filenames of HOCR files. Use '-' to read from stdin.");

    // Specify command line options -- resolution
    QCommandLineOption resolutionOption( {"r", "resolution"}, "Set resolution, <number> is in dots per inch. "
                                                              "This option is mandatory when writing PDF files, but not used for any other output file format.", "number");
    parser.addOption(resolutionOption);

    // Specify command line options -- output
    QCommandLineOption outputFileOption( {"o","output"},
                                         QString("Write output to <file>.  This argument is mandatory. "
                                                 "The ending of the file name determines the output file type. "
                                                 "Valid file types are Portable Document Format (PDF), text in UTF8 encoding (TXT) and graphics files (%1). "
                                                 "When saving graphics files, one file per page is generated. "
                                                 "These files are are numbered by appending four digits to the file name. "
                                                 "For instance, 'out.png' becomes 'out-0000.png', 'out-0001.png', etc.").arg(imageFormatEndings.join(", ")), "file");
    parser.addOption(outputFileOption);

    // Specify command line options -- title
    QCommandLineOption titleOption( {"t", "title"}, "Set <title> in metadata of output file. Defaults to 'unknown'. "
                                                    "This option is used when writing PDF and graphics files, but not used for any other output format.", "title");
    titleOption.setDefaultValue("unknown");
    parser.addOption(titleOption);

    // Specify command line options -- font
    QCommandLineOption fontOption("font", "Set font for use in output file. "
                                          "If no font is specified, a few standard fonts are tried, and the best-fitting font is used. "
                                          "This option is used when writing PDF and graphics files, but not used for any other output format.", "font");

    parser.addOption(fontOption);

    // Specify command line options -- overwrite
    QCommandLineOption forceOption( {"f", "force"}, "Overwrite existing files without asking.");
    parser.addOption(forceOption);

    // Specify command line options -- no warnings
    QCommandLineOption nwOption( {"w", "no-warnings"}, "Do not print warnings to stderr.");
    parser.addOption(nwOption);


    // Parse the actual command line arguments given by the user
    parser.process(app);


    /*
   * Evaluate results, and check for errors
   */

    // Check that output file is specified and that ending is known
    if (!parser.isSet(outputFileOption)) {
        qCritical().noquote() << wordwrap(QString("No output file specified. Use '-o' option to set. Aborting."));
        exit(-1);
    }
    QString outputFileName = parser.value(outputFileOption);
    QString outputFileNameEnding = outputFileName.section(".", -1);
    QString outputFileNameEndingU = outputFileNameEnding.toUpper();
    QString outputFileNameStem = outputFileName.section(".", 0, -2);
    if ((outputFileNameEndingU != "PDF") && (outputFileNameEndingU != "TXT") && !imageFormatEndings.contains(outputFileNameEndingU)) {
        qCritical().noquote() << wordwrap(QString("The output file format '%1' is unknown. "
                                                  "Use the '-h' option to see the list of supported formats. "
                                                  "Aborting.").arg(outputFileNameEnding));
        exit(-1);
    }

    // No positional argument → Error
    QStringList args = parser.positionalArguments();
    if (args.length() < 1)
        parser.showHelp(-1);

    // If specified, resolution needs to be within range.
    resolution overrideResolution;
    if (parser.isSet(resolutionOption)) {
        quint32 _resolution = 0;
        bool ok;
        _resolution = parser.value(resolutionOption).toUInt(&ok);
        if (!ok) {
            qCritical() << "Error: Cannot parse argument to resolution option.";
            exit(-1);
        }
        if ((_resolution < resolution::minResDPI) || (_resolution > resolution::maxResDPI)) {
            qCritical().noquote() << wordwrap(QString("Error. Resolution must be in range [%1..%2]. Aborting.").arg(resolution::minResDPI).arg(resolution::maxResDPI));
            exit(-1);
        }
        overrideResolution.set(_resolution, resolution::dpi);
    }


    /*
   * Read HOCR files and concatenate them
   */

    // Read all HOCR files, create a list of HOCRDocument
    auto documents = QtConcurrent::blockingMapped(args, loadHOCRDocument);

    // Check HOCRDocument files for errors. If no error, concatenate them.
    HOCRDocument hocrdoc;
    {
        int i=0;
        foreach(const HOCRDocument &document, documents) {
            // Handle errors
            if (document.hasError()) {
                qCritical().noquote() << wordwrap(QString("Error reading HOCR file '%1'. %2 Aborting.").arg(args[i],document.error()));
                exit(-1);
            }

            // Handle warnings
            if (document.hasWarnings() and !parser.isSet(nwOption)) {
                qWarning().noquote() << wordwrap(QString("There were warnings concerning the HOCR file '%1'.").arg(args[i])) << endl;
                foreach(const QString &warning, document.warnings())
                    qWarning().noquote() << wordwrap(warning) << endl;
            }

            // Append to main document
            hocrdoc.append(document);

            // Increase counter++
            i++;
        }
    }

    /*
   * Format = TXT? Then write text file
   */

    if (outputFileNameEndingU == "TXT") {
        // Warn if the user specified options that are meaningless for output format
        // text.
        if (!parser.isSet(nwOption)) {
            if (parser.isSet(resolutionOption))
                qWarning().noquote() << wordwrap("Warning: Resolution has been specified, but is not used for output format 'txt'.") << endl;
            if (parser.isSet(titleOption))
                qWarning().noquote() << wordwrap("Warning: Title has been specified, but is not used for output format 'txt'.") << endl;
            if (parser.isSet(fontOption))
                qWarning().noquote() << wordwrap("Warning: Font has been specified, but is not used for output format 'txt'.") << endl;
        }

        // Overwrite file warning
        if (QFile::exists(outputFileName) && !parser.isSet(forceOption)) {
            printf("File exists. Overwrite? [y/N] : ");
            auto inputChar = keypress();
            puts("");
            if ((inputChar != 'y') && (inputChar != 'Y')) {
                puts("Abort.");
                exit(0);
            }
        }

        // Write out file
        QFile data(outputFileName);
        if (data.open(QFile::WriteOnly)) {
            QTextStream out(&data);
            out << hocrdoc.toText();
        } else {
            qCritical().noquote() << wordwrap(QString("Error: Cannot write to output file '%1'. Aborting").arg(outputFileName));
            exit(-1);
        }
        data.close();
        return 0;
    }


    /*
   * Format = PDF? Then write PDF file and exit.
   */

    if (outputFileNameEndingU == "PDF") {
        // Check that a resolution has been set
        if (!parser.isSet(resolutionOption)) {
            qCritical().noquote() << wordwrap("Warning: Resolution has not been specified, but is required for output format 'PDF'. "
                                              "Use '-r' to specify.") << endl;
            exit(-1);
        }

        // Check if file exists
        if (QFile::exists(outputFileName) && !parser.isSet(forceOption)) {
            printf("File exists. Overwrite? [y/N] : ");
            auto inputChar = keypress();
            puts("");
            if ((inputChar != 'y') && (inputChar != 'Y')) {
                puts("Abort.");
                exit(0);
            }
        }

        // Generate font if need be
        QFont *myFont = nullptr;
        if (parser.isSet(fontOption))
            myFont = new QFont(parser.value(fontOption));

        // Write to file specified in 'outputFileName'
        QString error = hocrdoc.toPDF(outputFileName, overrideResolution, parser.value(titleOption), QPageSize(), myFont);
        delete myFont;

        // Error handling
        if (!error.isEmpty()) {
            qCritical().noquote() << wordwrap(QString("Error: Cannot write to output file '%1'. %2. Aborting").arg(outputFileName, error));
            exit(-1);
        }

        // Successfully done
        return 0;
    }


    /*
   * Format = Images? Then write image files
   */

    if (imageFormatEndings.contains(outputFileNameEndingU)) {
        // Generate font if need be
        QFont *myFont = nullptr;
        if (parser.isSet(fontOption))
            myFont = new QFont(parser.value(fontOption));

        // Generate stream of images
        QList<QImage> images = hocrdoc.toImages(myFont);
        delete myFont;

        // Set titles
        if (parser.isSet(titleOption)) {
            QString title = parser.value(titleOption);
            for(auto & image : images)
                image.setText("title", title);
        }

        // Create list of images and filenames
        QList<QPair<QImage, QString>> images_n_filenames;
        for(int i=0; i<images.size(); i++) {
            // Comput output file name
            QString outFileName = QString("%1-%2.%3").arg(outputFileNameStem).arg(i,4,10,QChar('0')).arg(outputFileNameEnding);

            // Check if file exists
            if (QFile::exists(outFileName) && !parser.isSet(forceOption)) {
                printf("File exists. Overwrite? [y/N] : ");
                auto inputChar = keypress();
                puts("");
                if ((inputChar != 'y') && (inputChar != 'Y')) {
                    puts("Abort.");
                    exit(0);
                }
            }

            images_n_filenames << qMakePair(images[i], outFileName);
        }

        // Save images and delete those which could successfully be saves
        QtConcurrent::blockingFilter(images_n_filenames, saveImage);

        // Handle errors
        for(auto & images_n_filename : images_n_filenames)
            qCritical().noquote() << wordwrap(QString("Error: Cannot write to output file '%1'. Aborting").arg(images_n_filename.second));
        if (!images_n_filenames.empty())
            exit(-1);

        return 0;
    }

    // This point should never be reached
    return 0;
}
