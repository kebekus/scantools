# Scantools

## Turn scanned documents into high-quality PDF files

scantools is a library and a matching set of command line applications for
graphics manipulation, written with a view towards the handling of scanned
documents and generation of high-quality PDF files.  

- Please see the [home page](https://kebekus.gitlab.io/scantools/) for more 
  information.

### Library functions

The library **libscantools** is written in C++ and makes heavy use of Qt5. Its
main features are the following.

* The class **PDFAWriter** generates well-crafted, PDF/A-2b compliant
  documents. Just construct a **PDFAWriter** instance, add graphic files and HOCR
  files to create a and well-crafted, searchable PDF file.  Files in JBIG2 and
  JPEG format, as well as JPEG2000 files in JPX format will be written directly
  into the PDF, all all other graphic files will be converted to RGB, and
  encoding losslessly in a way that depends on the image
  characteristics. Multi-page TIFF files are well supported.

* The class **HOCRDocument** reads and interprets HOCR files, the standard output
  file format for Optical Character Recognition systems.  It converts HOCR files
  to text, or renders them on any QPaintDevice.

### Command line utilities

At present, there are two command line utilities

* **image2pdf** converts images to PDF.  HOCR files, which are produced by optical
   character recognition programs such as **tesseract**, can optinonally be
   specified to make the PDF file searchable.  The resulting file complies with
   the ISO PDF/A standard for long-term archiving of digital documents.

* **hocr2any** converts HOCR files to text, or renders them as raster graphics or
  PDF files

* **ocrPDF** uses the tesseract OCR engine is used to detect text and to generate a
  text layer in the PDF file.


