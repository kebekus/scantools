# FindMuPDF
# ----------
#
# Try to find MuPDF
#
# Once done this will define
#
# ::
#
#   MuPDF_INCLUDE_DIR - the MuPDF include directory
#   MuPDF_LIBRARIES - The libraries needed to use MuPDF


#
# Find the necessary software libraries
#

# Find MuPDF library
FIND_PATH(MuPDF_INCLUDE_DIR mupdf/pdf.h )
FIND_LIBRARY(MuPDF_LIBRARY NAMES mupdf)
LIST(APPEND MuPDF_LIBRARIES ${MuPDF_LIBRARY})

# Find MuPDF third-party library, required for linking MuPDF
FIND_LIBRARY(MuPDF_third_LIBRARY NAMES mupdfthird)
IF(MuPDF_third_LIBRARY)
  LIST(APPEND MuPDF_LIBRARIES ${MuPDF_third_LIBRARY})
ENDIF()

# Find HarfBuzz library, required for linking MuPDF
FIND_PACKAGE(HarfBuzz)
LIST(APPEND MuPDF_LIBRARIES ${HarfBuzz_LIBRARIES})

# Find Z library, required for linking MuPDF
FIND_PACKAGE(ZLIB)
LIST(APPEND MuPDF_LIBRARIES ${ZLIB_LIBRARIES})

# Find Freetype library, required for linking MuPDF
FIND_PACKAGE(Freetype)
LIST(APPEND MuPDF_LIBRARIES ${FREETYPE_LIBRARIES})

# Find OpenJP2 library, required for linking MuPDF
FIND_PACKAGE(OpenJP2)
LIST(APPEND MuPDF_LIBRARIES ${OpenJP2_LIBRARIES})


#
# Put things together, handle arguments, check that everything is in place…
#
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MuPDF
  REQUIRED_VARS MuPDF_INCLUDE_DIR MuPDF_LIBRARY HarfBuzz_LIBRARIES ZLIB_LIBRARIES FREETYPE_LIBRARIES OpenJP2_LIBRARIES
  )
MARK_AS_ADVANCED(MuPDF_INCLUDE_DIR MuPDF_LIBRARIES)

