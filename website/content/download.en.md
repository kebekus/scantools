---
title: "Download"
description: "Download"
menu: main
---

## Binaries for Linux

We offer [packages for all common Linux
distributions](https://software.opensuse.org/download/package.iframe?project=home:stefan_kebekus&amp;package=scantools).
In addition to the binaries, the packages also include manual pages, development
files and API documentation.


## Binaries for other platforms

I expect that scantools should compile on all modern platforms, desktop or
mobile. Due to limited time, I support Linux only.


## Source code

The source code is hosted on GitLab, and is available from the [scantools GitLab
  page](https://gitlab.com/kebekus/scantools).
