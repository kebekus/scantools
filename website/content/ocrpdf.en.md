---
title: "ocrPDF"
description: "ocrPDF"
---

The program **ocrPDF** adds a text layer to a graphics-only PDF file. The
tesseract OCR engine is used for text detection.
