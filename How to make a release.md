This is a reminder for myself.

1. Check version number in src/CMakeLists.txt

2. Update ChangeLog

3. Create a release using the GIT-Lab site

4. Upload the API documentation to the web site

> ./buildscript-linux-release.sh
> rsync --verbose --delete --recursive build-release/src/libscantools/APIdoc/html/ kebekus@cplx.vm.uni-freiburg.de:/var/www/storage/scantools-api


5. Publish packages

> ./publish-linux-packages.sh
> ./publish-linux-snap.sh